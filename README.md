OpenShift base image
========================================

This repository contains Dockerfiles for images which serve as base images with all the
essential libraries and tools needed for OpenShift language images

This container image also installs several development libraries, that are
often required in the builder images above.
Sharing those development packages in a common layer saves disk space and
improves pulling speed.

Description
-----------

OpenShift S2I images use [Software Collections](https://www.softwarecollections.org/en/)
packages to provide the latest versions of various software.

WARNING
=======

This a fork.

- based on s2i-core-debian
- more minimal
- more clean

