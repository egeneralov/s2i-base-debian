FROM egeneralov/s2i-core-debian

ENV SUMMARY="Base image with essential libraries and tools used as a base for \
builder images like perl, python, ruby, etc." \
    DESCRIPTION="The s2i-base image, being built upon s2i-core, provides any \
images layered on top of it with all the tools needed to use source-to-image \
functionality. Additionally, s2i-base also contains various libraries needed for \
it to serve as a base for other builder images, like s2i-python or s2i-ruby."

LABEL summary="$SUMMARY" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="s2i base" \
      com.redhat.component="s2i-base-container" \
      name="egeneralov/s2i-base-debian" \
      version="1" \
      release="1" \
      maintainer="Eduard Generalov <eduard@generalov.net>"

# This is the list of basic dependencies that all language container image can
# consume.
RUN INSTALL_PKGS="gnupg2 apt-transport-https lsb-release ca-certificates build-essential automake bzip2 git lsof wget unzip" && \
  apt-get update -q && \
  apt-get install -yq ${INSTALL_PKGS} && \
  apt-get autoremove -yq && \
  apt-get autoclean -yq && \
  apt-get clean -yq
